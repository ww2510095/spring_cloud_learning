package org.ribbon.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class RibbonApplication {

    public static void main(String[] args) {
        SpringApplication.run(RibbonApplication.class, args);
    }

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(String key) {
        loadBalancerClient.choose("随便起个名字");//官方说这里是服务id，但是我发现随便起个名字都没事
        String skey = restTemplate.getForEntity("http://f1/test?key="+key, String.class).getBody();
        System.out.println(skey);
        return skey;

    }
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(String username,String password) {
        loadBalancerClient.choose("随便起个名字");//官方说这里是服务id，但是我发现随便起个名字都没事
        String skey = restTemplate.getForEntity("http://f1/login?username="+username+"&password="+password, String.class).getBody();
        System.out.println(skey);
        return skey;

    }

    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }


}
