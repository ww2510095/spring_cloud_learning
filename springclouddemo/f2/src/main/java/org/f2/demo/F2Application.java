package org.f2.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class F2Application {

    public static void main(String[] args) {
        SpringApplication.run(F2Application.class, args);
    }


    @RequestMapping(value = "/test" ,method = RequestMethod.GET)
    public String test(String key) {
        return "this is e f2----" + key;
    }

    @RequestMapping(value = "/login" ,method = RequestMethod.GET)
    public String login(String username,String password)
    {
        String sa =postlogin(username,password);
        if(sa.equals("登陆成功")){
            //这里把我们拿到的数据保存到数据库里面记录一下
            System.out.println("我是正确的账号:"+username);
            System.out.println("我是正确的密码:"+password);
        }
        return  sa;
    }
    //我们把这个方法当做是http方法
    String postlogin(String username,String password){
        if("123".equals(username)&&"123".equals(password))
            return "登陆成功" ;
        return  "账号或密码错误";
    }
}
