package org.f1.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class F1Application {

    public static void main(String[] args) {
        SpringApplication.run(F1Application.class, args);
    }


    @RequestMapping(value = "/test" ,method = RequestMethod.GET)
    public String test(String key) {
        return "this is e f1----" + key;
    }

    @RequestMapping(value = "/login" ,method = RequestMethod.GET)
    public String login(String username,String password)
    {
        if("123".equals(username)&&"123".equals(password))
            return "登陆成功" ;
        return  "账号或密码错误";
    }
}
